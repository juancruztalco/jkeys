#! /usr/bin/env node

/**
 * Created by JuanCruz on 06/09/2015.
 */
var colors = require('colors');
var files   = require("./files");
var args    = require("./args");
var add    = require("./add");

var p = args.src || "./";

console.log('Executing jkeys...\n'.green);

var data = files.read( p );
var counter = 0;

if (args.find) {

    console.log("\nSearching %s", args.find);

    for ( var file in data ){

        if ( args.find in data[file] ) {
            counter++;
            console.log("Key %s founded in file %s : %s", args.find, file, data[file][args.find] );
        }


    }
    if ( counter == 0){
        console.log("The key %s is not present in this files".red, args.find)
    }
}


if ( args.add ){
    files.write( add( args.add, data ) )
}