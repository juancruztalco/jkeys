/**
 * Created by JuanCruz on 06/09/2015.
 */

var fs      = require("fs");
var path    = require("path");

var files = {
    read: function( p ){
        var jsonFiles = [],
            response = {};

        var files = fs.readdirSync( p );

        files.map(function (file) {
            return path.join(p, file);
        }).filter(function (file) {
            return fs.statSync(file).isFile();
        }).forEach(function (file) {

            if ( path.extname(file) == ".json" ){
                jsonFiles.push( file );
            }
        });

        if ( jsonFiles.length <= 0){
            console.log("No json files found".red)
            process.exit()
        }

        console.log("Founded %s files".green, jsonFiles.length)

        jsonFiles.forEach(function( file ){
            console.log("%s", file);
            var data = fs.readFileSync(file, 'utf8');
            response[file]=JSON.parse(data);
        });

        return response;
    },
    write : function( data ){

        for ( var file in data ){

            fs.writeFileSync(file, JSON.stringify(data[file], null, 4));
            console.log("File %s updated".green, file);
        }

    }
}

module.exports = files;