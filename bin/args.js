/**
 * Created by JuanCruz on 06/09/2015.
 */

var commandLineArgs = require("command-line-args");

var cli = commandLineArgs([
    { name: "src", alias: "s", type: String,  defaultOption: true },
    {
        name : "help",
        alias : "h",
        type : Boolean,
        defaultOption : false
    },
    {
        name : "find",
        alias : "f",
        type : String
    },
    {
        name : "add",
        alias : "a",
        type : String
    }

    /*{ name: "timeout", alias: "t", type: Number, multiple: true }*/
]);

var options = cli.parse();
var usage =  cli.getUsage({
    title: "Jkeys",
    description: "Edit multiple json keys at the same time",
    footer: "Project home: [underline]{https://juancruztalco@bitbucket.org/juancruztalco/jkeys.git}"
});

if ( options.help === true ) {
    console.log( usage );
    process.exit()
}

module.exports = options;