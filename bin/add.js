/**
 * Created by JuanCruz on 06/09/2015.
 */
var read = require('readline-sync');
var colors = require('colors');

var add = function( key, data ){

    var result;
    var opts = ['Override'];

    for ( var file in data ){

        if ( key in data[file] ) {
            console.log("\nKey '%s' already exists in file '%s' with content: '%s'".red, key, file, data[file][key] );
            index = read.keyInSelect(opts, 'What do you wanna do?');
            if ( index == -1) {
                console.log("Skipping file %s".green, file)
                continue;
            }
        }

        result = read.question("Enter content for key '" + key + "' in file '" + file + "': ");

        data[file][key] = result;

    }

    return data;

}

module.exports = add;