# Jkeys #

Small library to handle the content of multiple json files with the same keys in the console

### Overview ###

* Version 0.1.0

### Install ###

```
#!command

$ npm install -g jkeys

```
### Options ###

**--help, -h**

Show availables options

**--src, -s**

Defines path where the search will be made. All json files in the path will be processed. Actual path wil be used ff not specified.

Example: 

```
#!command

jkeys -s languages
```



**--find, -f**

Search for a specific key in all the files

Example: 
```
#!command

jkeys -s languages -f HELLO
```
Output: 
```
#!command

Key HELLO founded in file languages\en.json : Hello
Key HELLO founded in file languages\es.json : Hola

```

**--add, -a**

Add a new key and its content to all the files. If the key already exist, skip or replace options will be prompted.

Example: 
```
#!command

jkeys -s languages -a HELLO
```

